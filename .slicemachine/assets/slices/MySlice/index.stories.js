import MyComponent from '../../../../slices/MySlice';
import SliceZone from 'vue-slicezone'

export default {
  title: 'slices/MySlice'
}


export const _Default = () => ({
  components: {
    MyComponent,
    SliceZone
  },
  methods: {
    resolve() {
      return MyComponent
    }
  },
  data() {
    return {
      mock: {"variation":"default","name":"Default","slice_type":"my_slice","items":[],"primary":{"title":[{"type":"heading1","text":"E-enable cross-media convergence","spans":[]}],"description":[{"type":"paragraph","text":"Voluptate elit aliqua Lorem dolor laborum excepteur ut cupidatat enim laboris et laboris nisi quis. Esse est cupidatat deserunt deserunt nostrud tempor ut consectetur ut nostrud in mollit. Anim aute consequat sunt.","spans":[]}],"image":{"dimensions":{"width":900,"height":500},"alt":"Placeholder image","copyright":null,"url":"https://images.unsplash.com/photo-1547082299-de196ea013d6?w=900&h=500&fit=crop"},"color":"#ad4985"},"id":"_Default"}
    }
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />'
})
_Default.storyName = 'Default'
